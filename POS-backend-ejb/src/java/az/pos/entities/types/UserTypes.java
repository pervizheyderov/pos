package az.pos.entities.types;

/**
 *
 * @author o2_u1_pfk
 */
public enum UserTypes {
    WAITER("WAITER"), ADMIN("ADMIN"), CASHIER("CASHIER"), KITCHEN("KITCHEN");
    private final String types;

    private UserTypes(String types) {
        this.types = types;
    }

    public String getTypes() {
        return types;
    }

}
