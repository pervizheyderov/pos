package az.pos.entities.types;

/**
 *
 * @author o2_u1_pfk
 */
public enum UserStatus {
    ACTIVE("ACTIVE"), BLOCKED("BLOCKED");
    private final String status;

    private UserStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
