/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.pos.managerbeans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author o2_u1_pfk
 */
@ManagedBean
@SessionScoped
public class MenuController {
    private  int currentMenuItem;
    
    @PostConstruct
    public void init (){
        currentMenuItem = 1;
    }
    public void changeMenuItem(int i){
        currentMenuItem = i;
    }

    public  int getCurrentMenuItem() {
        return currentMenuItem;
    }

    public  void setCurrentMenuItem(int currentmenuItem) {
        this.currentMenuItem = currentmenuItem;
    }
    
}
