package az.pos.managerbeans;

import az.pos.entities.User;
import az.pos.entities.ejbs.UserFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author o2_u1_pfk
 */
@ManagedBean
public class UserManager {

    private User user;
    private List<User> users;

    @PostConstruct
    public void init() {

    }

    @EJB
    private UserFacadeLocal userFacade;

    public void addUser() {
        userFacade.create(user);
    }

    public void removeUser(User u) {
        try {
            userFacade.remove(u);
            showMessage("Succesfully Deleted", null , FacesMessage.SEVERITY_INFO);
        } catch (Exception e) {
            showMessage("Something went wrong", e.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }

    public void showMessage(String message, String detail, FacesMessage.Severity severity) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, message, detail));
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUsers() {
        users = userFacade.findAll();
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
